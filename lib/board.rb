class Board
  attr_reader :grid
  def initialize(grid = [[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid = grid
  end

  def place_mark(pos,mark)
    @grid[pos[0]][pos[1]] = mark
  end
  def print_grid
    (0..@grid.length).each do |num|
      puts @grid[num][0..-1]
    end
  end
  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end


  def winner
    #three ways of winning, all hard coding
    #columns
    if @grid[0][0] == @grid[0][1] && @grid[0][1] == @grid[0][2] && grid[0][0] != nil
      return @grid[0][0]
    elsif @grid[1][0] == @grid[1][1] && @grid[1][1] == @grid[1][2] && grid[1][0] != nil
      return @grid[1][0]
    elsif @grid[2][0] == @grid[2][1] && @grid[2][1] == @grid[2][2] && grid[2][0] != nil
      return @grid[2][0]
    #rows
    elsif @grid[0][0] == @grid[1][0] && @grid[1][0] == @grid[2][0] && grid[0][0] != nil
      return @grid[0][0]
    elsif @grid[0][1] == @grid[1][1] && @grid[1][1] == @grid[2][1] && grid[0][1] != nil
      return @grid[0][1]
    elsif @grid[0][2] == @grid[1][2] && @grid[1][2] == @grid[2][2] && grid[0][2] != nil
      return @grid[0][2]
    #right diagonal
    elsif @grid[0][0] == @grid[1][1] && @grid[2][2] == @grid[1][1] && @grid[0][0] != nil
      return @grid[0][0]
    #left diagonal
    elsif @grid[2][0] == @grid[1][1] && @grid[1][1] == @grid[0][2] && @grid[1][1] != nil
      return @grid[2][0]
    else
      return nil
    end
  end

  def tied?
    @grid.all? do |box|
      box.all?{|ele| ele != nil}

    end
  end

  def over?
    if winner == :X ||  winner == :O
      return true
    elsif tied?
      return true
    end
    false
  end

end

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :playerone, :playertwo, :board, :cur_player,:cur_mark
  def initialize(player_one,player_two)
    @playerone = player_one
    @playertwo = player_two
    @board = Board.new
    @cur_player = player_one
    @cur_mark = :X
  end

  def board
    @board
  end

  def current_player
    @cur_player
  end

  def switch_players!
    if @cur_mark == :O
      @cur_mark = :X
      @cur_player = playerone
    else
      @cur_mark = :O
      @cur_player = playertwo
    end
  end

  def play_turn
    move = @cur_player.get_move
    @board.place_mark(move,@cur_mark)
    switch_players!
  end

end

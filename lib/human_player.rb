class HumanPlayer
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def get_move
    puts "where do you want to move"
    inputz = gets
    inputz = inputz.split(', ').map!(&:to_i)
    puts inputz
    inputz
  end

  def display(board)
    (0..2).each do |num1|
      ans = ""
      (0..2).each do |num2|
        if board.grid[num1][num2] == nil
          ans += '  '
        else
          ans += ' ' + board.grid[num1][num2].to_s
        end
      end
      puts ans
    end
  end
end

class ComputerPlayer
  attr_reader :name, :board, :marker
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def mark=(marker)
    @marker = marker
  end

  def get_move
    if winning_move != nil
      return winning_move
    else
      #choose random move
      chosen = 0
      while chosen == 0
        widz = rand(3)
        lenz = rand(3)
        if @board.grid[lenz,widz] == nil
          return [lenz,widz]
          chosen = 1
        end
      end
    end
  end

  def board
    @board
  end

  def winning_move
    (0..2).each do |num|
      #column
      if @board.grid[num][0] == @marker && @board.grid[num][1] == @marker
        return [num,2]
      elsif @board.grid[num][2] == @marker && @board.grid[num][1] == @marker
        return [num,0]
      elsif @board.grid[num][2] == @marker && @board.grid[num][0] == @marker
        return [num,1]
      #row
      elsif @board.grid[0][num] == @marker && board.grid[1][num] == @marker
        return [2,num]
      elsif @board.grid[0][num] == @marker && board.grid[2][num] == @marker
        return [1,num]
      elsif @board.grid[2][num] == @marker && board.grid[1][num] == @marker
        return [0,num]
      end
    end
    #right diagonal
    if @board.grid[0][0] == @marker && @board.grid[2][2] == @marker
      return [1][1]
    elsif @board.grid[1][1] == @marker && @board.grid[2][2] == @marker
      return [0][0]
    elsif @board.grid[0][0] == @marker && @board.grid[1][1] == @marker
      return [2][2]
    end
    #opp diagonal
    if @board.grid[0][2] == @marker && @board.grid[2][0] == @marker
      return [1][1]
    elsif @board.grid[1][1] == @marker && @board.grid[0][2] == @marker
      return [2][0]
    elsif @board.grid[2][0] == @marker && @board.grid[1][1] == @marker
      return [0][2]
    end
    return nil
  end
end
